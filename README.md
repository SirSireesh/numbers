# Numbers

This is a D library that allows easy manipulation of numbers, and offers with it many convenience functions.

It is mostly meant for Project Euler and similar programming puzzles.