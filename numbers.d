module numbers;
import std.traits : isIntegral, Unsigned, isUnsigned;
import std.functional : memoize;
import std.range : isInputRange;

/++
`isPrime` checks if a number n is prime

Returns `true` if n is a prime

Allows any `n` given that `n` is an integral and that `n` supports the `^^` operator
Params:
n = is the number to test against
+/

bool isPrime(T)(in T n)
if (isIntegral!T && isUnsigned!T)
{
	if (n == 2 || n == 3 || n == 5)
		return true;
	if (n % 2 == 0 || n % 3 == 0 || n % 5 == 0)
		return false;

	for (ulong i = 5; i < (n ^^ 0.5) + 1; i += 6) {
		if (n % i == 0 || n % (i + 2) == 0)
			return false;
	}
	return true;
}

///
@safe pure nothrow @nogc unittest
{
	static assert (isPrime(11u) && !isPrime(10u));
	static assert (!isPrime(9u));
}

/++
Returns a sorted random access range containing all primes in range [2, max_prime)

Params:
T = the type of integers to be returned. It must be unsigned
max_prime = the number which the largest prime in the array should be less than.
max_prime must be greater than 2.
+/
auto primeSieve(T = ulong, U = ulong)(in U max_prime)
if (isIntegral!T && isUnsigned!T)
in {
	assert(max_prime > 2);
} do {
	import std.range : assumeSorted;
	T[] nums = [2, 3, 5, 7];

	if (!__ctfe)
		nums.reserve(max_prime);

	for (uint i = 11; i < max_prime; i += 2)
		if (isPrime(i))
			nums ~= i;

	return nums.assumeSorted;
}

///
@safe pure nothrow unittest
{
	import std.algorithm : equal;
	auto primes100 = primeSieve(100);
	assert(primes100[$ - 1] == 97);

	enum primes24 = primeSieve(24);
	static assert (equal(primes24, [2UL, 3, 5, 7, 11, 13, 17, 19, 23]));
}

/++
Returns a sorted random access range containing all primes in range [min_prime, max_prime)

Params:
max_prime = the number which the largest prime in the array should be less than.
max_prime must be greater than 2.
min_prime = the number which the smallest prime should be grater than or equal to
min_prime must be less than max_prime
+/

auto primeSieve(T = ulong, U = ulong)(in U min_prime, in U max_prime)
if (isIntegral!T && isUnsigned!T)
in {
	assert(max_prime > 2);
	assert(min_prime < max_prime, "min_prime must be less than max_prime!");
} do {
	import std.range : assumeSorted;
	T[] nums;

	if (!__ctfe)
		nums.reserve(max_prime - min_prime);

	for (ulong i = min_prime % 2 ? min_prime : min_prime + 1; i < max_prime; i += 2) {
		if (isPrime(i))
			nums ~= i;
	}

	return nums.assumeSorted;
}

///
@safe pure nothrow unittest
{
	import std.range : isRandomAccessRange;
	auto primes = primeSieve(3, 100);
	static assert (isRandomAccessRange!(typeof(primes)));
	assert(primes[0] == 3 && primes[$ - 1] == 97);
}

/++
Memoized fibonacci function.

Useful if there will be many repetetive calls to calculate the n'th fibonacci term

Params:
n = is the term which must be calculated
+/
ulong fibMemoize(in ulong n) @safe nothrow
{
	alias fibMem = memoize!fibMemoize;

	if (n == 0)
		return 0;
	else if (n == 1)
		return 1;
	else
		return fibMem(n - 1) + fibMem(n - 2);
}

///
@safe nothrow unittest
{
	immutable tenth = fibMemoize(10u);
	assert(tenth == 55);
	immutable ninth = fibMemoize(9u); //Memoized, does not recompute
	assert (fibMemoize(10u) == fib(10u));
}

/++
Regular Fibonacci function

Useful of there will be rare calls with small values.
Uses a simple loop to calculate the value

Params:
T = the type. Must be numeric.
n = is the term which must be calculated
+/
T fib(T)(T n)
if (isIntegral!T && isUnsigned!T)
{
	T x = 0, y = 1, z = 1;
	foreach (i; 0 .. n) {
		x = y;
		y = z;
		z = x + y;
	}
	return x;
}

///
@safe pure nothrow @nogc unittest
{
	immutable tenth = fib(10u);
	assert(tenth == 55);
	assert (fib(1u) == 1);
}

/++
Returns a sorted random access range containing all fibonacci terms in the range [0, n)

Params:
n = the nth term upto which numbers must be generated
+/
auto fibonacciSieve(T = ulong)(in ulong n)
if (isIntegral!T && isUnsigned!T)
{
	import std.range : assumeSorted;
	immutable(T)[] list;
	if (!__ctfe)
		list.reserve(n);
	auto x = 0, y = 1, z = 1;
	foreach (i; 0 .. n) {
		list ~= x;
		x = y;
		y = z;
		z = x + y;
	}
	return list.assumeSorted;
}

///
@safe pure nothrow unittest
{
	auto fibonacci = fibonacciSieve(6u);
	assert(fibonacci[$ - 1] == 5);

	import std.algorithm : equal;
	enum fibSeive = fibonacciSieve(6u);
	static assert (equal(fibSeive, [0UL, 1, 1, 2, 3, 5]));
}

/++
Regular factorial function

Useful of there will be rare calls with small values.
Uses a simple loop to calculate the value

Params:
T = the type. Must be numeric.
n = is the term which must be calculated
+/
T factorial(T)(in T n)
if (isIntegral!T && isUnsigned!T)
{
	if (n <= T(1))
		return T(1);
	T fact = T(n);
	foreach (i; T(2) .. n) {
		fact *= i;
	}
	return fact;
}

///
@safe pure nothrow @nogc unittest
{
	assert (factorial(10u) == 36_28_800);
	static assert (factorial(5u) == 120u && factorial(0u) == 1u);
}

/++
Memoized factorial function.

Useful if there will be many repetetive calls to calculate the n`th factorial term

Params:
n = is the term which must be calculated
+/
ulong factMemoize(in ulong n) @safe nothrow
{
	alias factMem = memoize!factMemoize;

	return n <= 1 ? 1 : n * factMem(n - 1);
}

///
@safe nothrow unittest
{
	assert (factMemoize(10u) == 36_28_800);
	immutable nine = factMemoize(9); //Memoized, does not recompute

	assert (factorial(10u) == factMemoize(10u));
}

/++
Returns a sorted random access range containing all factorial terms in the range [0, n)

Params:
n = the nth term upto which numbers must be generated
+/
auto factorialSieve(T = ulong)(in size_t n)
if (isIntegral!T && isUnsigned!T)
{
	import std.range : assumeSorted;
	immutable(T)[] list = [1];
	if (!__ctfe)
		list.reserve(n);
	foreach (i; 1 .. n)
		list ~= i * list[i - 1];
	return list.assumeSorted;
}

///
@safe pure nothrow unittest
{
	import std.algorithm : equal;
	enum facts = factorialSieve(10);
	assert(facts[$ - 1] == 362_880);
	static assert (equal(facts, [1UL, 1, 2, 6, 24, 120, 720, 5040, 40320, 362880]));
}

/++
Returns the lcm of the two numbers

Params:
a = first number
b = second number
+/

T lcm(T)(T a, T b)
if (isIntegral!T && isUnsigned!T)
{
	return (a * b) / gcd(a, b);
}

///
@safe pure nothrow @nogc unittest
{
	static assert (lcm(12u, 24u) == 24);
	static assert (lcm(3u, 5u) == 15);
}

/++
Returns the gcd of the two numbers

Params:
a = first number
b = second number
+/

T gcd(T)(T a, T b)
if (isIntegral!T)
{
	return a == 0 ? b : gcd(b % a, a);
}

///
@safe pure nothrow @nogc
{
	static assert (gcd(36u, 60u) == 12);
}

/++
A range that takes a number and iterates over the digits of the number
Returns:
A bidirectional range, with elements comprising of the number given
Note:
The numbers are iterated from left to right, i.e. `12345u.digitRange.front` will be `1`, not `5`
+/
auto digitRange(T)(T num)
if(isIntegral!T && isUnsigned!T)
{
	static struct DigitRange(T)
	if (isIntegral!T && isUnsigned!T)
	{
		private {
			import std.container : Array;
			ubyte[] _nums;
			ubyte * _ptr;
		}

		///
		this(T num) nothrow
		{
			ubyte _length;
			for (auto tmp = num; tmp; tmp /= 10)
				++_length;
			() @trusted @nogc {
				import core.stdc.stdlib : malloc;
				_ptr = cast(ubyte *) malloc(ubyte.sizeof * _length);
				_nums = _ptr[0 .. _length];
			}();
			for (auto tmp = num; tmp; tmp /= 10)
				_nums[--_length] = tmp % 10;
		}

		~this() @trusted
		{
			import core.stdc.stdlib : free;
			if (_ptr != null)
				free(_ptr);
			_ptr = null;
		}

		///
		this(this) @safe
		{
			ubyte[] tmp;
			() @trusted {
				import core.stdc.stdlib : malloc;
				_ptr = cast(ubyte *)malloc(ubyte.sizeof * _nums.length);
				tmp = cast(ubyte[]) _ptr[0 .. _nums.length];
			}();
			tmp[] = _nums[];
			_nums = tmp;
		}

		///
		bool empty() const @nogc @safe pure nothrow @property
		{
			return _nums.length == 0;
		}

		///
		bool opEquals(ref DigitRange!(T) rhs) const pure
		{
			return this._nums == rhs._nums;
		}

		///
		ref ubyte opIndex(ulong index) pure @nogc
		{
			return _nums[index];
		}

		///
		DigitRange!T opAssign(ref DigitRange!T rhs)
		{
			() @trusted {
				import core.stdc.stdlib : malloc;
				_ptr = cast(ubyte *)malloc(ubyte.sizeof * _nums.length);
				_nums = cast(ubyte[]) _ptr[0 .. _nums.length];
			}();
			this._nums[] = rhs._nums[];
			return rhs;
		}

		///
		ref ubyte front() pure @nogc
		{
			return _nums[0];
		}

		///
		void popFront() @nogc
		{
			if (_nums.length > 0)
				_nums = _nums[1 .. $];
		}

		///
		ref ubyte back() pure @nogc
		{
			return _nums[$ - 1];
		}

		///
		void popBack() @nogc
		{
			if (_nums.length > 0)
				_nums = _nums[0 .. $ - 1];
		}

		///
		auto save() const @nogc
		{
			return DigitRange!T(this.toNumber);
		}

		///
		ulong length() const @safe pure nothrow @nogc @property
		{
			return _nums.length;
		}

		///
		string toString() const pure @safe
		{
			import std.conv : to;
			return _nums.to!string;
		}

		ulong opDollar()
		{
			return length;
		}

		/++
		Returns:
		the underlying number that is being looked at by the digit range
		+/
		T toNumber() const
		{
			T _num;
			foreach (i, n; _nums)
				_num += n * 10 ^^ (length - i - 1);
			return _num;
		}
	}
	return DigitRange!T(num);
}

///
@safe nothrow @nogc unittest
{
	import std.range : isRandomAccessRange;

	auto dr = digitRange(12345U);
	static assert (isRandomAccessRange!(typeof(dr)));

	assert (dr.front == 1);
	assert (dr.back == 5);
	assert (dr.save == dr);
	assert (dr.length == 5);
}

///
auto digitRange(R, T = ulong)(scope R range)
if (isInputRange!R/+ && is(ElementType!R : ubyte) && isIntegral!T && isUnsigned!T+/)
{
	import std.range : enumerate, walkLength;
	T num;
	auto _copy = range;
	ulong _length = _copy.walkLength;
	foreach (i, n; range.enumerate) {
		num += n * (10 ^^ (_length - i - 1));
	}
	return digitRange(num);
}

///
@safe nothrow @nogc unittest
{
	ubyte[3] nums = [1u, 2u, 3u];
	assert (nums[].digitRange.toNumber == 123);
}

/++
A simple and straightforward representation of ratios
+/
struct Ratio {
	@safe: pure: nothrow:
	/// The numerator
	long numerator;
	/// The denominator
	long denominator;
	///
	this(long numerator, long denominator = 1) @nogc
	{
		this.numerator = numerator;
		this.denominator = denominator;
	}

	///Supports addition, subtraction, multiplication and division
	Ratio opBinary(string op)(Ratio rhs)
	{
		Ratio result;
		static if (op == "+") {
			result.numerator = this.numerator * rhs.denominator + rhs.numerator * this.denominator;
			result.denominator = rhs.denominator * this.denominator;
			result.normalize();
			return result;
		} else static if (op == "-") {
			result.numerator = this.numerator * rhs.denominator - rhs.numerator * this.denominator;
			result.denominator = rhs.denominator * this.denominator;
			result.normalize();
			return result;
		} else static if (op == "*") {
			result.numerator = this.numerator * rhs.numerator;
			result.denominator = this.denominator * rhs.denominator;
			result.normalize();
			return result;
		} else static if (op == "/") {
			result.numerator = this.numerator * rhs.denominator;
			result.denominator = this.denominator * rhs.numerator;
			result.normalize();
			return result;
		} else static assert (0, "unsupported operation on Ratios : '" ~ op ~ "'!");
	}

	///Supports addition, subtraction, multiplication and division
	Ratio opBinary(string op)(long rhs) const
	{
		Ratio result;
		static if (op == "+") {
			result.numerator = this.numerator + rhs * this.denominator;
			result.denominator = this.denominator;
			result.normalize();
			return result;
		} else static if (op == "-") {
			result.numerator = this.numerator - rhs * this.denominator;
			result.denominator = this.denominator;
			result.normalize();
			return result;
		} else static if (op == "*") {
			result.numerator = this.numerator * rhs;
			result.denominator = this.denominator;
			result.normalize();
			return result;
		} else static if (op == "/") {
			result.numerator = this.numerator;
			result.denominator = this.denominator * rhs;
			result.normalize();
			return result;
		} else static assert (0, "unsupported operation on Ratios : '" ~ op ~ "'!");
	}

	Ratio opBinaryRight(string op)(long rhs) const
	{
		Ratio result;
		static if (op == "+") {
			result.numerator = this.numerator + rhs * this.denominator;
			result.denominator = this.denominator;
			result.normalize();
			return result;
		} else static if (op == "-") {
			result.numerator = this.numerator - rhs * this.denominator;
			result.denominator = this.denominator;
			result.normalize();
			return result;
		} else static if (op == "*") {
			result.numerator = this.numerator * rhs;
			result.denominator = this.denominator;
			result.normalize();
			return result;
		} else static if (op == "/") {
			result.numerator = this.denominator * rhs;
			result.denominator = this.numerator;
			result.normalize();
			return result;
		} else static assert (0, "unsupported operation on Ratios : '" ~ op ~ "'!");
	}

	private void normalize() @nogc
	{
		if (denominator < 0) {
			numerator *= -1;
			denominator *= -1;
		}
	}

	bool opEquals(Ratio rhs) @nogc const
	{
		rhs.normalize();
		Ratio lhs = this;
		lhs.normalize();
		if (lhs.numerator == rhs.numerator && lhs.denominator == rhs.denominator)
			return true;
		return false;
	}

	/// Raduce the ratio to its simplest form, i.e. numerator and denominator are now coprimes
	Ratio simplify() @nogc
	{
		long g;
		while ((g = gcd(numerator, denominator)) != 1) {
			numerator /= g;
			denominator /= g;
		}
		return this;
	}

	/// numerator / denominator
	string toString() const
	{
		import std.conv : to;
		return numerator.to!string ~ "/" ~ denominator.to!string;
	}
}

///
@safe pure nothrow @nogc unittest
{
	import std.conv : to;
	import std.stdio : writeln;
	Ratio r1 = Ratio(1, 2), r2 = Ratio(2, 3);
	assert(r1 + r2 == Ratio(7, 6), "ratio addition failed!");
	auto r3 = r1 * r2;
	assert(r3 == Ratio(2, 6), "ratio multiplication failed!");
	assert(r3.simplify() == Ratio(1, 3), "ratio simplification failed!");
	r1 = r1 * -1;
	assert(r1 == Ratio(-1, 2), "ratio multiplication with integers failed!");
	r3 = r1 * r2;
	assert(r3.simplify() == Ratio(-1, 3), "ratio simplification failed!");
	assert(r3 + 1 == Ratio(2, 3), "ratio addition with integers failed!");
	assert(1 / r3 == Ratio(3, -1), "ratio inversion failed!");
}
